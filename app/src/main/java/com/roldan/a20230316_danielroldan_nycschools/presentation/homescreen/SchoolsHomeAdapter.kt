package com.roldan.a20230316_danielroldan_nycschools.presentation.homescreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.roldan.a20230316_danielroldan_nycschools.databinding.SchoolListitemViewBinding
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School

typealias SchoolClickListener = (School) -> Unit

class SchoolsHomeAdapter(
    private val onSchoolClickListener: SchoolClickListener
) : ListAdapter<School, SchoolsHomeAdapter.SchoolViewHolder>(SchoolDiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SchoolViewHolder(
            SchoolListitemViewBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bindItem(getItem(position))
    }

    inner class SchoolViewHolder(private val itemBinding: SchoolListitemViewBinding) :
        ViewHolder(itemBinding.root) {
        fun bindItem(school: School) {
            with(itemBinding) {
                dbn.text = school.dbn
                schoolName.text = school.name
                schoolLocation.text = school.location
                schoolDetailsBtn.setOnClickListener { onSchoolClickListener(school) }
            }
        }
    }
}

class SchoolDiffCallBack : DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }
}