package com.roldan.a20230316_danielroldan_nycschools.presentation.homescreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.roldan.a20230316_danielroldan_nycschools.core.utils.ResultAPI
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import com.roldan.a20230316_danielroldan_nycschools.domain.repository.SchoolRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// HomeViewModel is responsible for managing the data for the home screen.
class HomeViewModel(
    private val repository: SchoolRepository
) : ViewModel() {

    // MutableLiveData to manage the list of schools.
    private val _schoolList = MutableLiveData<List<School>>()

    // LiveData to expose the list of schools.
    val schoolList: LiveData<List<School>> get() = _schoolList

    // LiveEvent to manage navigation to school details.
    private val _detailsNavigate = LiveEvent<School>()

    // LiveData to expose navigation to school details.
    val detailsNavigate: LiveData<School> get() = _detailsNavigate

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> get() = _error

    // Load schools when the ViewModel is instantiated.
    init {
        loadSchools()
    }

    // Load the list of schools from the repository.
    private fun loadSchools() {
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = repository.getAllSchools()) {
                is ResultAPI.Error -> {
                    result.message?.let { error ->
                        _error.postValue(error)
                    }
                }
                is ResultAPI.Success -> {
                    // Update the list of schools upon successful API call.
                    result.data.let {
                        _schoolList.postValue(it)
                    }
                }
            }
        }
    }

    // Trigger navigation to the school details when a school is clicked.
    fun onSchoolClicked(school: School) {
        _detailsNavigate.postValue(school)
    }
}