package com.roldan.a20230316_danielroldan_nycschools.presentation.details

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.roldan.a20230316_danielroldan_nycschools.core.utils.ResultAPI
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import com.roldan.a20230316_danielroldan_nycschools.domain.models.SchoolScores
import com.roldan.a20230316_danielroldan_nycschools.domain.repository.SchoolRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailsViewModel(
    private val repository: SchoolRepository
): ViewModel() {

    private val _schoolScores = MutableLiveData<SchoolScores>()
    val schoolScores: LiveData<SchoolScores> get() = _schoolScores

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> get() = _error

    fun loadSatScores(schoolSelected: School) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repository.getSchoolScores(schoolSelected.dbn)
            result?.let {result ->
                when(result){
                    is ResultAPI.Error -> {
                        result.message?.let { error ->
                            _error.postValue(error)
                        }
                    }
                    is ResultAPI.Success -> {
                        result.data?.let { data ->
                            _schoolScores.postValue(data)
                        }
                    }
                }
            }
        }
    }
}