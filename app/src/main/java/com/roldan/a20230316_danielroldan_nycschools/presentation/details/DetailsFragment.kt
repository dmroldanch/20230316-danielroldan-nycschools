package com.roldan.a20230316_danielroldan_nycschools.presentation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.roldan.a20230316_danielroldan_nycschools.databinding.SchoolDetailsScreenBinding
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import com.roldan.a20230316_danielroldan_nycschools.domain.models.SchoolScores
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailsFragment : Fragment() {
    private val viewModel: DetailsViewModel by viewModel()
    private lateinit var binding: SchoolDetailsScreenBinding
    private val args by navArgs<DetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SchoolDetailsScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showSchool(args.schoolSelected)
        viewModel.loadSatScores(args.schoolSelected)
        viewModel.schoolScores.observe(viewLifecycleOwner){ scores ->
            scores?.let {it ->
                showScores(it)
            }
        }

        viewModel.error.observe(viewLifecycleOwner){ error ->
            error?.let { message ->
                Toast.makeText(activity,message,Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun showSchool(school: School){
        with(binding){
            detailsDbn.text = school.dbn
            detailsName.text = school.name
            detailsEmail.text = school.email
            detailsLocation.text= school.location
            detailsPhone.text = school.phone
            detailsWeb.text = school.website
        }
    }

    private fun showScores(scores: SchoolScores){
        with(binding){
            detailsMathScore.progress = scores.mathScore / 10
            detailsReadingScore.progress =  scores.readingScore  / 10
            detailsWritingScore.progress =  scores.writingScore / 10
        }
    }

}