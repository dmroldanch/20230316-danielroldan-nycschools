package com.roldan.a20230316_danielroldan_nycschools.presentation.homescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.roldan.a20230316_danielroldan_nycschools.databinding.SchoolsMainScreenBinding
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var binding: SchoolsMainScreenBinding

    private val schoolAdapter = SchoolsHomeAdapter(::onSchoolSelected)

    private fun onSchoolSelected(school: School) {
        viewModel.onSchoolClicked(school)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SchoolsMainScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.schoolsRv.adapter = schoolAdapter
        viewModel.schoolList.observe(viewLifecycleOwner) { schools ->
            schoolAdapter.submitList(schools)
        }
        viewModel.detailsNavigate.observe(viewLifecycleOwner) { school ->
            findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToDetailsFragment(
                    school
                )
            )
        }
        viewModel.error.observe(viewLifecycleOwner){ error ->
            error?.let {message  ->
                Toast.makeText(activity,message, Toast.LENGTH_SHORT).show()

            }
        }
    }
}