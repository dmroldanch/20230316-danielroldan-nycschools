package com.roldan.a20230316_danielroldan_nycschools.data.remote.services

import com.roldan.a20230316_danielroldan_nycschools.data.remote.models.SchoolItemResponse
import com.roldan.a20230316_danielroldan_nycschools.data.remote.models.SchoolScoresResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {

    @GET("resource/s3k6-pzi2.json")
    suspend fun fetchAllSchools(): Response<List<SchoolItemResponse>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSchoolScores(
        @Query("dbn") dbn: String
    ): Response<List<SchoolScoresResponse>>
}