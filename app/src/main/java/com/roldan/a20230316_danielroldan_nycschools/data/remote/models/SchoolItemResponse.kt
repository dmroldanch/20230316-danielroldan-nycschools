package com.roldan.a20230316_danielroldan_nycschools.data.remote.models

data class SchoolItemResponse(
    val dbn : String? = "",
    val school_name : String? = "",
    val overview_paragraph : String? = "",
    val location : String? = "",
    val phone_number : String? = "",
    val school_email : String? = "",
    val website : String? = ""
)