package com.roldan.a20230316_danielroldan_nycschools.data.remote.mappers

import com.roldan.a20230316_danielroldan_nycschools.data.remote.models.SchoolItemResponse
import com.roldan.a20230316_danielroldan_nycschools.data.remote.models.SchoolScoresResponse
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import com.roldan.a20230316_danielroldan_nycschools.domain.models.SchoolScores

fun SchoolItemResponse.toSchool() : School{
    return School(
        dbn = dbn.toString(),
        name = school_name.toString(),
        location = location.toString(),
        overview = overview_paragraph.toString(),
        phone = phone_number.toString(),
        email = school_email.toString(),
        website = website.toString()
    )
}


fun SchoolScoresResponse.toSchoolScores() : SchoolScores {
    return SchoolScores(
    testTaken = num_of_sat_test_takers?.toIntOrNull() ?: 0,
    mathScore = sat_math_avg_score?.toIntOrNull() ?: 0,
    readingScore = sat_critical_reading_avg_score?.toIntOrNull() ?: 0,
    writingScore = sat_writing_avg_score?.toIntOrNull() ?: 0
    )
}

