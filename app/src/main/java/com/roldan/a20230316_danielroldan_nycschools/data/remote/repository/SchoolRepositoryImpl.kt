package com.roldan.a20230316_danielroldan_nycschools.data.remote.repository

import com.roldan.a20230316_danielroldan_nycschools.core.utils.ResultAPI
import com.roldan.a20230316_danielroldan_nycschools.data.remote.mappers.*
import com.roldan.a20230316_danielroldan_nycschools.data.remote.services.SchoolsApi
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import com.roldan.a20230316_danielroldan_nycschools.domain.models.SchoolScores
import com.roldan.a20230316_danielroldan_nycschools.domain.repository.SchoolRepository

class SchoolRepositoryImp(
    private val api: SchoolsApi
) : SchoolRepository {
    override suspend fun getAllSchools(): ResultAPI<List<School>> {
        val response = api.fetchAllSchools()

        return when (response.isSuccessful) {
            true -> {
               ResultAPI.Success(response.body()?.map { it.toSchool() } ?: emptyList())
            }
            false -> {
               ResultAPI.Error(response.message().toString())
            }
        }

    }

    override suspend fun getSchoolScores(schoolDbn: String): ResultAPI<SchoolScores?> {
        val response = api.fetchSchoolScores(schoolDbn)
        return when (response.isSuccessful) {
            true -> {
                ResultAPI.Success(response.body()?.firstOrNull()?.toSchoolScores())
            }
            false -> {
                ResultAPI.Error(response.message().toString())
            }
        }
    }
}