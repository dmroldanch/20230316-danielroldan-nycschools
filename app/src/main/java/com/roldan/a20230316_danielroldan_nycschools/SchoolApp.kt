package com.roldan.a20230316_danielroldan_nycschools

import android.app.Application
import com.roldan.a20230316_danielroldan_nycschools.core.di.networkModule
import com.roldan.a20230316_danielroldan_nycschools.core.di.repositoryModule
import com.roldan.a20230316_danielroldan_nycschools.core.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class SchoolApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidLogger()
            androidContext(this@SchoolApp)
            modules(networkModule, repositoryModule, viewModelModule)
        }

    }

}