package com.roldan.a20230316_danielroldan_nycschools.core.utils

sealed class ResultAPI<T> {
    data class Success<T>(val data : T) : ResultAPI<T>()
    data class Error<T>(val message  : String) : ResultAPI<T>()
}
