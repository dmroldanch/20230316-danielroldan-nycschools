package com.roldan.a20230316_danielroldan_nycschools.core.di

import com.google.gson.Gson
import com.roldan.a20230316_danielroldan_nycschools.BuildConfig
import com.roldan.a20230316_danielroldan_nycschools.data.remote.services.SchoolsApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { providesApi(get()) }
    single { providesOkHttpClient() }
    single { providesParser() }
    single { providesRetrofit(get(),get()) }
}

fun providesOkHttpClient() : OkHttpClient = OkHttpClient.Builder()
    .apply {
        if(BuildConfig.DEBUG){
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
        }
    }
    .build()

fun providesParser(): Gson = Gson()

fun providesRetrofit(
        okHttpClient: OkHttpClient,
        converter: Gson
    ): Retrofit = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(converter))
        .build()

fun providesApi(
        retrofit: Retrofit
): SchoolsApi = retrofit.create(SchoolsApi::class.java)
