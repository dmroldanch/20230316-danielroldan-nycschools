package com.roldan.a20230316_danielroldan_nycschools.core.di

import com.roldan.a20230316_danielroldan_nycschools.presentation.details.DetailsViewModel
import com.roldan.a20230316_danielroldan_nycschools.presentation.homescreen.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel{ HomeViewModel(get())}
    viewModel{ DetailsViewModel(get())}
}