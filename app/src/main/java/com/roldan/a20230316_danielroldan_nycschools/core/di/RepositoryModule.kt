package com.roldan.a20230316_danielroldan_nycschools.core.di

import com.roldan.a20230316_danielroldan_nycschools.data.remote.repository.SchoolRepositoryImp
import com.roldan.a20230316_danielroldan_nycschools.domain.repository.SchoolRepository
import org.koin.dsl.module

val repositoryModule = module {

    single<SchoolRepository>{
        SchoolRepositoryImp(get())
    }
}