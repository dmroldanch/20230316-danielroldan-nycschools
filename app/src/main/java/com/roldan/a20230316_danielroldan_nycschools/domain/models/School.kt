package com.roldan.a20230316_danielroldan_nycschools.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val dbn: String = "",
    val name: String = "",
    val location: String = "",
    val overview: String = "",
    val phone: String = "",
    val email: String = "",
    val website: String = ""
): Parcelable
