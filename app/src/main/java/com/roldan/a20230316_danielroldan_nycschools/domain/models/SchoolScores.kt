package com.roldan.a20230316_danielroldan_nycschools.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolScores(
    val testTaken: Int = 0,
    val readingScore: Int= 0,
    val mathScore: Int = 0,
    val writingScore: Int = 0,
): Parcelable
