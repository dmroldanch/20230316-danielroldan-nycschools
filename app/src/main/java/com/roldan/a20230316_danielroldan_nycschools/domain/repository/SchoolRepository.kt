package com.roldan.a20230316_danielroldan_nycschools.domain.repository

import com.roldan.a20230316_danielroldan_nycschools.core.utils.ResultAPI
import com.roldan.a20230316_danielroldan_nycschools.domain.models.School
import com.roldan.a20230316_danielroldan_nycschools.domain.models.SchoolScores

interface SchoolRepository {

    suspend fun getAllSchools(): ResultAPI<List<School>>
    suspend fun getSchoolScores(schoolDbn: String): ResultAPI<SchoolScores?>

}